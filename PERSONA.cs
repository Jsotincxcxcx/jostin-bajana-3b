﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Jostin_Bajaña3B
{
    class PERSONA
    {
        public string Nombres { get; set; }
        public string Apellidos { get; set; }
        public string NumeroDeCedula { get; set; }
        public string EstadoCivil { get; set; }

        public PERSONA(string Nombres, string Apellidos, string NumeroDeCedula, string EstadoCivil)
        {
            this.Nombres = Nombres;
            this.Apellidos = Apellidos;
            this.NumeroDeCedula = NumeroDeCedula;
            this.EstadoCivil = EstadoCivil;
        }
        public void ReasignarEstadoCivil(string Estadocivil)
        {
            EstadoCivil = Estadocivil;
        }
    }
}
