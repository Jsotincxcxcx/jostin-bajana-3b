﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Jostin_Bajaña3B
{
    class Estudiante : PERSONA

    {
        public int Curso { get; set; }
        public Estudiante(int Curso, string Nombres, string Apellidos, string NumeroDeCedula, string EstadoCivil) : base(Nombres, Apellidos, NumeroDeCedula, EstadoCivil)
        {
            this.Curso = Curso;
        }
        public void matriculacion(int Curso)
        {
            this.Curso = Curso;
        }
        public void Mostrardatos()
        {
            Console.WriteLine("Datos del empleado:\nNombre y Apellido: " + Nombres + " " + Apellidos);
            Console.WriteLine("El numero de cedula es : " + NumeroDeCedula );
            Console.WriteLine("Estado civil : " + EstadoCivil );
            Console.WriteLine("El curso es:" + Curso );
        }
    }
}