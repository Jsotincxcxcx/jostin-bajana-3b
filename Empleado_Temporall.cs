﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Jostin_Bajaña3B
{
            class EmpleadoTemporal : Empleado
        {
            public new int Id;
            public DateTime FechaDeIngreso
            {
                get; set;
            }
            public DateTime FechaDeSalida
            {
                get; set;
            }
            public int sueldo = 800;

            public EmpleadoTemporal(DateTime FechaDeIngreso, DateTime FechaDeSalida, string Apellidos, string Nombres, int Edad, string Departamento) : base(Apellidos, Nombres, Edad, Departamento)
            {
                this.FechaDeIngreso = FechaDeIngreso;
                this.FechaDeSalida = FechaDeSalida;
            }
                //Procesos de calculo
            public float CalcularSueldo()
            {
                TimeSpan diferenciaDeFechas = FechaDeSalida - FechaDeIngreso;
                int Dias = diferenciaDeFechas.Days;
                int Meses = Dias / 30;
                float TotalSueldo = Meses * sueldo;
                return TotalSueldo;
            }
        }
    }
