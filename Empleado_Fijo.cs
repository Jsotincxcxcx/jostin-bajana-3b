﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Jostin_Bajaña3B
{
    class Empleado_Fijo : Empleado
    {
        public DateTime AñoDeEntrada { get; set; }
        static private float sueldomensual = 800;
        public int MesesDeTrabajo = 10;

        public Empleado_Fijo(DateTime AñoDeEntrada, string Apellidos, string Nombres, int Edad, string Departamento) : base(Apellidos, Nombres, Edad, Departamento)
        {
            this.AñoDeEntrada = AñoDeEntrada;
        }
        public float ProcesoSueldo()
        {
            float sueldo = MesesDeTrabajo * sueldomensual;
            return sueldo;
        }
    }
}
