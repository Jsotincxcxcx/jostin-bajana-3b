﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Jostin_Bajaña3B
{
    class Empleado
    {
        public string Apellidos { get; set; }
        public string Nombres { get; set; }
        public int Edad { get; set; }
        public string Departamento { get; set; }

        public Empleado(string apellidos, string nombres, int Edad, string Departamento)
        {
            Apellidos = apellidos;
            this.Nombres = nombres;
            this.Edad = Edad;
            this.Departamento = Departamento;
        }
    }
}
