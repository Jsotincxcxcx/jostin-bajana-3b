﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Jostin_Bajaña3B
{
    class Personade_Servicio : Empleadoss
    {
        public string sección { get; set; }
        public Personade_Servicio(string Sección, int Despacho, DateTime FechaDeIngreso, string Nombres, string Apellidos, string NumeroDeCedula, string EstadoCivil) : base(Despacho, FechaDeIngreso, Nombres, Apellidos, NumeroDeCedula, EstadoCivil)
        {
            this.sección = Sección;
        }
        public void trasladodeseccion(string seccion)
        {
            this.sección = seccion;
        }
        public void Mostrardatos()
        {
            Console.WriteLine("Datos del empleado:\nNombre y Apellido: " + Nombres + " " + Apellidos);
            Console.WriteLine("El número de cedula es: " + NumeroDeCedula);
            Console.WriteLine("Estado civil: " + EstadoCivil);
            Console.WriteLine("La fecha de ingreso es : " + FechaDeIngreso);
            Console.WriteLine("La sección es : " + sección);
           
        }
    }
}