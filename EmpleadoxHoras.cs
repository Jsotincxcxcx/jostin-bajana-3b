﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Jostin_Bajaña3B
{
    class EmpleadoxPorHora : Empleado

    {
        static private float HoraPrecio = 6.10f;
        public int HorasDeTrabajo { get; set; }

        public EmpleadoxPorHora(int HorasDeTrabajo, string Apellidos, string Nombres, int Edad, string Departamento) : base(Apellidos, Nombres, Edad, Departamento)
        {
            this.HorasDeTrabajo = HorasDeTrabajo;
        }
        public float ProcesoSueldo()
        {
            float sueldo = HoraPrecio * HorasDeTrabajo;
            return sueldo;
        }
    }
}
