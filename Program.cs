﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Jostin_Bajaña3B
{
    class Program
    {
            public static void Main(string[] args)
        {
            string datosEmpleado = "";


            Empleado_Fijo empleadoFijo = new Empleado_Fijo(new DateTime(2001, 10, 10), "Bajaña", "Pancrasio", 46, "bodega");

            datosEmpleado = "Nombre: " + empleadoFijo.Nombres + "\nApellidos: " + empleadoFijo.Apellidos + "\nSueldo:" + empleadoFijo.ProcesoSueldo();
            Console.WriteLine(datosEmpleado);
          

            EmpleadoTemporal empleadoTemporal = new EmpleadoTemporal(new DateTime(2008, 5, 1, 8, 30, 52), new DateTime(2009, 5, 1, 8, 30, 52), "Bajaña", "Pancrasio", 46, "Bodega");
            datosEmpleado = "Nombre: " + empleadoTemporal.Nombres + "\nApellidos: " + empleadoTemporal.Apellidos + "\nSueldo:" + empleadoTemporal.CalcularSueldo();
            

            Estudiante estudiante = new Estudiante(1, "Rodolfo", "Rodriguez", "1312345678", "Divorciado");
            DOCENTE profesor = new DOCENTE("Sistemas", 2, new DateTime(2008, 5, 1, 8, 30, 52), "Rodolfo", "Rodriguez", "1312345789", "Divorciado");

            Personade_Servicio personaDeServicio = new Personade_Servicio("bliblioteca", 2, new DateTime(2008, 5, 1, 8, 30, 52), "Rodolfo", "Rodriguez", "1312345678", "Divorciado");
            estudiante.Mostrardatos();
            profesor.Mostrardatos();
            personaDeServicio.Mostrardatos();
            Console.ReadKey();
        }
    }
}
