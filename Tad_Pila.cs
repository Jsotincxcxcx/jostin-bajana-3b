﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Jostin_Bajaña3B
{
        class Tad_Pila
        {
            public static int Extensio = 4;
            public static int[] Pila = new int[Extensio];
            public static int CIMA = -1;

            public Boolean Vacia()
            {
                return (Pila.Length - 1) == CIMA;
            }
            public void Apilar(int dato)
            {
                if (CIMA < Extensio)
                {
                    CIMA++;
                    Pila[CIMA] = dato;
                }

            }

            public int Desapilar()
            {
                int valor = Tope();
                CIMA--;
                return valor;
            }

            public int Tope()
            {
                return Pila[CIMA];
            }
        }
    }
