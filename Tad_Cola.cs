﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Jostin_Bajaña3B
{
    class Tad_Cola
    {
        public static int Extension = 8;
        public int[] Cola = new int[Extension];
        public int Primer = -1;
        public int ultimo = -1;

        public Boolean Vacia()
        {
            return (Primer == -1 & ultimo == -1);
        }
        public void Encolar(int dato)
        {
            if (Vacia())
            {
                Primer++;
                ultimo++;
                Cola[ultimo] = dato;
            }
        }

        public int Desencolar()
        {
            int valor = Cola[Primer];
            Primer++;
            if (Primer == Extension)
            {
                Primer = 0;
            }

            return valor;
        }

        public int Ultimo()
        {
            return Cola[ultimo];
        }
    }
}
