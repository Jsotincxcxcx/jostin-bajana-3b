﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Jostin_Bajaña3B
{
    class DOCENTE : Empleadoss
    {
        public string Departamento { get; set; }
        public DOCENTE(string Departamento, int Despacho, DateTime FechaDeIngreso, string Nombres, string Apellidos, string NumeroDeCedula, string EstadoCivil) : base(Despacho, FechaDeIngreso, Nombres, Apellidos, NumeroDeCedula, EstadoCivil)
        {
            this.Departamento = Departamento;
        }
        public void Cambiodedepartamento(string departamento)
        {
            Departamento = departamento;
        }


        public void Mostrardatos()
        {
            Console.WriteLine("Datos del empleado:\nNombre y Apellido: " + Nombres + " " + Apellidos + "\n" +
            "Numero de cedula: " + NumeroDeCedula + "\n" +
            "Estado civil: " + EstadoCivil + "\n" +
            "La Fecha de ingreso es: " + FechaDeIngreso + "\n" +
             "Pertenece al Departamento: " + Departamento);
        }
    }
}
